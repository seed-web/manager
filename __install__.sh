#changer le répertoire courant de l'utilisateur  www-data

sudo service nginx stop
sudo service php7.4-fpm stop

sudo usermod -d /home/www www-data

sudo service php7.4-fpm start
sudo service nginx start

ssh-keyscan -t rsa github.com | sudo tee /home/www/.ssh/known_hosts 
ssh-keyscan -t rsa framagit.org | sudo tee /home/www/.ssh/known_hosts 

# ERROR: 
#message peut pas ajouter le serveur dans known_host
#marche plus si on ajoute le serveur
#ssh-keyscan -t rsa 176.9.183.75 | sudo tee /home/www/.ssh/known_hosts 

