
<h2>liste des projets</h2>


<?php

//-------------------------------------------------------
function project_control($dir){
//-------------------------------------------------------
    global $MAIN_PATH;
    if(is_dir($dir."/.git")){
    //$url=command_result(" && git config --get remote.origin.url",$dir)[0];

        $path=str_replace($MAIN_PATH,"", $dir);
        ?>
        <tr>
        <td class="url"><?=$path?></td>
        <td><?=project_link("go",$dir)?></td>
        <td><?=project_view("details","project.php",$dir)?></td>

        <td><?=project_button("push","push",$dir)?></td>
        <td><?=project_button("pull","pull",$dir)?></td>

        </tr>
        <?php
    }
}
//-------------------------------------------------------
function scanAllDir($dir) {
//-------------------------------------------------------
    $result = [];
    foreach(scandir($dir) as $filename) {

        if ($filename[0] === '.') continue;


        $filePath = $dir . '/' . $filename;
        if (is_dir($filePath)){

            if( is_dir($filePath."/.git")) {
                $result[] = $filePath;

            } else {
                foreach (scanAllDir($filePath) as $childFilename) {
                    $result[] = $childFilename;
                }
            }
        }
    }
    return $result;
}
//-------------------------------------------------------
?>
<table>
<?php
foreach( scanAllDir($MAIN_PATH) as $dir){

    project_control($dir);
}
?>
</table>

