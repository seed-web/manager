<a href="."><button>retour</button></a>
<?php
include('./templates/menubar.php');
//-------------------------------------------------------

$action=$_POST["action"];


if($action=="clone"){
    $git_url=$_POST["git_url"];
    $subdir=$_POST["subdir"];

    $command="bash ./scripts/clone.sh ".$git_url." ".$MAIN_PATH."/".$subdir;

}else if($action=="pull"){
    $dir=$_POST["dir"];
    $command="cd ".$dir." && git pull";

}else if($action=="push"){
    $dir=$_POST["dir"];
    $command="bash ./scripts/push.sh ".$dir;

}else if($action=="delete"){
    $dir=$_POST["dir"];
    $command="rm -R ".$dir;

}else if($action=="mkdir"){
    $name=$_POST["name"];
    $command="mkdir -p ".$MAIN_PATH."/".$name;

}else if($action=="pull-all"){
    $command="bash ./scripts/pull-all.sh ".$MAIN_PATH;

}else if($action=="push-all"){
    $command="bash ./scripts/push-all.sh ".$MAIN_PATH;


}else if($action=="keygen"){
    $command="bash ./scripts/keygen.sh";

}else if($action=="plot-keygen"){
    $command="bash ./scripts/plot-keygen.sh";


}else if($action=="git-conf"){
    $name=$_POST["name"];
    $email=$_POST["email"];
    $command="bash ./scripts/git-conf.sh ".$name." ".$email;

}

echo "<h2>".$command."</h2>";

$output=command_result($command);

foreach($output as $line){
    echo "<p>".$line."</p>";
}

//-------------------------------------------------------
?>


