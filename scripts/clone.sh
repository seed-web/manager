#/bin/bash

REPOSITORY="$2"

git_url="$1"
name=${git_url%.*}

mkdir -p $REPOSITORY

if [ ! -d "$REPOSITORY$name" ]
then
    cd $REPOSITORY && git clone $git_url 

fi

    echo $REPOSITORY
    echo $(realpath .)
