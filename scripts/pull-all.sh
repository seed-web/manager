#/bin/bash



for elt in $(find "$(realpath $1)" -type d -name ".git")
do
    echo $elt
    cd $(dirname $elt) && git pull
done

