#/bin/bash



for elt in $(find "$(realpath $1)" -type d -name ".git")
do

    cd $(dirname $elt)
    git add -A
    git commit -m "auto"
    git push
done

